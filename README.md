Installation Instructions:  

1 Change the directory to where the "Labyrinth Linux" is located  
2.if you do not have SDL2, SDL2_image and SDL2_ttf, install them on your machine    
3.make install  
  
Controls:  
Left-Click to the tile you want to move the player to.  
Left-Click to the spot you want to move the sliding tile into.  
Right-Click to rotate the tile.    
Space bar to slide the tile in    
Escape to exit the game  
  
This game is in early development and may or may not be worked on in the future. 
  
Terms Of Use:  
The software provided by this repository is offered as is with no guarantees of validity, reliability, or safety and for playing the game which is provided by the software.  
You are welcome to look at and modify the files, but not for  use in any derivations which you will gain monetarily from without written consent from the developers.    
The developers of the software will not be responsible for any damages that arise as a result of installing, playing, or otherwise accessing or downloading the game, though we don't anticipate any issues as there were no such issues during development.  
By downloading and installing this software, you agree to be bound by these terms.  