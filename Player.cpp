/*
     Author: Marcus Galeotti
     Purpose: Implementation of player object to represent a player of the game
     Date of Last Update: 10/28/17
*/

#include "Player.hpp"
void Player::setPlayerVals(const int xVal, const int yVal, const int width, const int height)
{
    playerRect.x = xVal;
    playerRect.y = yVal;
    playerRect.w = width;
    playerRect.h = height;
}
